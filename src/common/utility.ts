export class Utilities {

    public GetFormattedBirthdate(dateString: string) {
        return dateString.split('-')[2] + "/" + dateString.split('-')[1] + "/" + dateString.split('-')[0];
    }
    public GetAttribute(name: string, value: any) {
        var attribute = {
            name: name,
            value: value,
        };
        return attribute;
    }
}