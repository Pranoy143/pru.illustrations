export class EducareRuntimeRequest {
    public body: body
    constructor(transaction:string,role:string,channel:string) {
        this.body=new body(transaction,role,channel)
    }
}
class body {
    public transactionContext: transactionContext
    public products: Array<any>
    constructor(transaction:string,role:string,channel:string)
    {
        this.transactionContext=new transactionContext(transaction,role,channel)
        this.products=new Array<any>()
    }
}
class transactionContext {
    public transaction: string
    public transactionRole: string
    public transactionChannel: string
    constructor(transaction:string,role:string,channel:string) {
        this.transaction = transaction
        this.transactionRole =role
        this.transactionChannel = channel
    }
}
export class product{
    public code:string
    public attributes:Array<any>
    public components:Array<any>
    constructor (code:string)
    {
        this.code=code
        this.attributes=new Array<any>()
        this.components=new Array<any>()
    }
}