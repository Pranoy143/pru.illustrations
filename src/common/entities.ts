export class SharedEntities {
    public educare: string = "S00471"
    public riders: Rider
    public illustrationType:IllustrationType
    constructor() {
        this.riders = new Rider()
        this.illustrationType=IllustrationType.LA1
    }

}
class Rider {
    pcbRider: string = "S00282"
    fibRider: string = "S00283"
    deathRider: string = "S00309"
    tpdRider: string = "S00312"
    ac50Rider: string = "S00285"
    ad50Rider: string = "S00286"
    wopRider: string = "S00287"
    safetyRider: string = "S00288"
}

export enum IllustrationType {LA1,LA2}