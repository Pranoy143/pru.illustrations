import { productDefinitionHelper } from "./test/service-helper/productDefinition-helper";
export class runtimeFacade {
    public _request: any
    public _dataConnector: any
    private _prodDefnAggregator:any

    constructor(request: any, dataConnector: any) {
        this._request = request;
        this._dataConnector = dataConnector;
        this._prodDefnAggregator=new productDefinitionHelper();
    }
    public async GetPremium() {
        try {
            let prodDefArray = await this._prodDefnAggregator.GetSummarisedProductDefs("DIGITAL", this._request.products[0].code);
            var getPremiumResponse = await this._prodDefnAggregator.callRuntime(this._request, this._dataConnector, true, "PREMIUM",prodDefArray);
            return getPremiumResponse;
        }
        catch (err) {
            console.log(err);
            return err;
        }
    }

    public async GetPlans() {
        try {
            let prodDefArray = await this._prodDefnAggregator.GetSummarisedProductDefs("DIGITAL", this._request.products[0].code);
            var getPlansResponse = await this._prodDefnAggregator.callRuntime(this._request, this._dataConnector, false, "F_GetPlans",prodDefArray);
            return getPlansResponse;
        }
        catch (err) {
            console.log(err);
            return err;
        }
    }
}






