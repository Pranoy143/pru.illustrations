import {SharedEntities,IllustrationType} from "../common/entities";

export class ResponseMapper {
    public sharedEntities: SharedEntities
    constructor() {
        this.sharedEntities = new SharedEntities()
    }
    public mapEducareResponse(educareResponse:any,runtimeResponse: any,illustrationType: IllustrationType) {
        if (runtimeResponse != null) {
            if (illustrationType == IllustrationType.LA1) {
                var basePlanInfo = runtimeResponse["products"][0]["tables"][0]["planInfo"];
                basePlanInfo.forEach((element: {
                    term: string;totalPremiumPaid: any;premiumValue: any;surrenderValue: any;totalSumAssured: any;totalSumAssuredAccident: any;
                }) => {
                    educareResponse["totalPremiumPaid(".concat(element.term, ")")] = element.totalPremiumPaid;
                    educareResponse["premiumPaidEveryYear(".concat(element.term, ")")] = element.premiumValue;
                    educareResponse["guaranteedSurrenderValue(".concat(element.term, ")")] = element.surrenderValue;
                    educareResponse["sumAssuredAccident(".concat(element.term, ")")] = element.totalSumAssured;
                    educareResponse["sumAssuredNonAccident(".concat(element.term, ")")] = element.totalSumAssuredAccident;
                    educareResponse["totalelcDeathBenefit(".concat(element.term, ")")] = element.totalSumAssured;
                    educareResponse["totalelcDeathBenefitAccidental(".concat(element.term, ")")] = element.totalSumAssuredAccident;
                });
                educareResponse["totalModalPremium"] = basePlanInfo[0].premiumValue;
                educareResponse["modalPremium"] = basePlanInfo[0].premiumValue;
                educareResponse["discountedPremium"] = basePlanInfo[0].premiumValue;
                educareResponse["totalPremiumWithLevy"] = basePlanInfo[0].premiumValue;
                educareResponse["educareAnnualyPremium"] = basePlanInfo[0].premiumValue;
                educareResponse["annualyTpAfterDisc"] = basePlanInfo[0].premiumValue;
                educareResponse["annualyTpBeforeDisc"] = basePlanInfo[0].premiumValue;
                educareResponse["educareSumAssured"] = basePlanInfo[0].totalSumAssured;
                educareResponse["requestedAt"] = Date.now();
            }
            var riderInformation = runtimeResponse["products"][0]["components"];
            let identifier=(illustrationType==IllustrationType.LA1)?"la1":"la2";
            riderInformation.forEach((element: {
                [x: string]: {
                    [x: string]: any;
                } [];code: any;
            }) => {
                if (element["tables"] != null) {
                    var riderPlanInfo = element["tables"][0]["planInfo"];
                    switch (element.code) {
                        case this.sharedEntities.riders.pcbRider:
                            educareResponse[identifier+"PCBAnnualyPremium"] = riderPlanInfo[0].premiumValue;
                            educareResponse[identifier+"PCBSumAssuredAccidental"] = riderPlanInfo[0].totalSumAssuredAccident;
                            break;
                        case this.sharedEntities.riders.fibRider:
                            educareResponse[identifier+"FIBAnnualyPremium"] = riderPlanInfo[0].premiumValue;
                            educareResponse[identifier+"FIBSumAssuredAccidental"] = riderPlanInfo[0].totalSumAssuredAccident;
                            if(illustrationType==IllustrationType.LA1){
                            riderPlanInfo.forEach((element: {
                                term: string;totalSumAssured: any;totalSumAssuredAccident: any;
                            }) => {
                                educareResponse["FIBAccident(".concat(element.term, ")")] = element.totalSumAssured;
                                educareResponse["FIBNonAccident(".concat(element.term, ")")] = element.totalSumAssuredAccident;
                            });
                        }
                            break;
                        case this.sharedEntities.riders.safetyRider:
                            educareResponse[identifier+"PRUTAnnualyPremium"] = riderPlanInfo[0].premiumValue;
                            educareResponse[identifier+"PRUTSumAssuredAccidental"] = riderPlanInfo[0].totalSumAssuredAccident;
                            educareResponse[identifier+"PRUTSumAssured"] = riderPlanInfo[0].totalSumAssured;
                            break;
                    }
                }
            });
        }

        return educareResponse;
    }
}