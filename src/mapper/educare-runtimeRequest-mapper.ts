import {SharedEntities,IllustrationType} from "../common/entities";
import {Utilities} from "../common/utility";
import {EducareRuntimeRequest,product} from "../common/educare-runtimeRequest";
export class RuntimeRequestMapper {
    public getEducareIllustrationsRequest(vpmsRequest:any,illustrationType:IllustrationType) {
        var request =new EducareRuntimeRequest("SALES","ANY","ALL");
        var utilities=new Utilities();
        var sharedEntities=new SharedEntities();
        var educareProduct =new product(sharedEntities.educare);
        let identifier=(illustrationType==IllustrationType.LA1)?"LA1":"LA2";
        let insuredIdentifier=(illustrationType==IllustrationType.LA1)?"assured":"secondlifeAssured"
        let premiumFrequency = vpmsRequest["paymentModeNoun"].toUpperCase() == "YEAR" ? "Annually" : "Monthly";
        educareProduct.attributes.push(utilities.GetAttribute("premiumFrequency", premiumFrequency)); //Mapping of Premium Frequency-TBD
        educareProduct.attributes.push(utilities.GetAttribute("policyTerm", vpmsRequest["period"]));
        educareProduct.attributes.push(utilities.GetAttribute("paymentMode", "NonCash")); //Mapping of Payment Mode-TBD
        educareProduct.attributes.push(utilities.GetAttribute("totalSumAssured", ((illustrationType==IllustrationType.LA1)?vpmsRequest["insuredAmount"]:0)));       
        //RISK
        var riskRider =new product("S00304");
        riskRider.attributes.push(utilities.GetAttribute("birthDate", utilities.GetFormattedBirthdate(vpmsRequest[insuredIdentifier]["dateOfBirth"])));
        riskRider.attributes.push(utilities.GetAttribute("gender", vpmsRequest[insuredIdentifier]["gender"]));
        educareProduct.components.push(riskRider);
        //PCB
        var pcbRider =new product(sharedEntities.riders.pcbRider);
        pcbRider.attributes.push(utilities.GetAttribute("isSelected", vpmsRequest["riders"]["PCB"]["educarecheck"+identifier]));
        educareProduct.components.push(pcbRider);
        educareProduct.attributes.push(utilities.GetAttribute("pcbTotalSumAssured", vpmsRequest["riders"]["PCB"]["educareinsuredAmount"+identifier]));
        //FIB
        var fibRider = new product(sharedEntities.riders.fibRider);
        fibRider.attributes.push(utilities.GetAttribute("isSelected", vpmsRequest["riders"]["FIB"]["educarecheck"+identifier]));
        educareProduct.components.push(fibRider);
        educareProduct.attributes.push(utilities.GetAttribute("fibTotalSumAssured", vpmsRequest["riders"]["FIB"]["educareinsuredAmount"+identifier]));
        //Safety+
        var safetyRider =new product(sharedEntities.riders.safetyRider);
        safetyRider.attributes.push(utilities.GetAttribute("isSelected", vpmsRequest["riders"]["PRUT"]["educarecheck"+identifier]));
        educareProduct.components.push(safetyRider);
        //CI AC50
        var ac50Rider = new product(sharedEntities.riders.ac50Rider);
        ac50Rider.attributes.push(utilities.GetAttribute("isSelected", vpmsRequest["riders"]["CI"]["ciType"] == "AC50"));
        educareProduct.components.push(ac50Rider);
        //CI AD50
        var ad50Rider =new product(sharedEntities.riders.ad50Rider);
        ad50Rider.attributes.push(utilities.GetAttribute("isSelected", vpmsRequest["riders"]["CI"]["ciType"] == "AD50"));
        educareProduct.components.push(ad50Rider);
        //CI WOP
        var wopRider =new product(sharedEntities.riders.wopRider);
        wopRider.attributes.push(utilities.GetAttribute("isSelected", vpmsRequest["riders"]["CI"]["educareccwop"+identifier].toUpperCase() == "INCLUDED"));
        wopRider.attributes.push(utilities.GetAttribute("totalSumAssured", vpmsRequest["insuredAmount"]));
        educareProduct.components.push(wopRider);
        request.body.products.push(educareProduct);
        return request;
    }
}