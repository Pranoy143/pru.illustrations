import {ResponseMapper} from "./mapper/educare-mapper";
import {RuntimeRequestMapper} from "./mapper/educare-runtimeRequest-mapper";
const {ProductMasterFactory} = require("@product-master-sdk/product-master-lib");
import {SharedEntities, IllustrationType} from "./common/entities";
import {MockResponse} from "./vpmsResponse/educare-response";
export class Illustrations {
    public _vpmsRequest: any
    public _runtimeRequestMapper: RuntimeRequestMapper
    public _responseMapper: ResponseMapper
    public _dataConnector:any
    public _sharedEntities:SharedEntities
    public _mockResponse:MockResponse
    constructor(request: any,dataConnector:any) {
        this._vpmsRequest = request;
        this._runtimeRequestMapper = new RuntimeRequestMapper();
        this._responseMapper = new ResponseMapper();
        this._dataConnector=dataConnector;
        this._sharedEntities=new SharedEntities();
        this._mockResponse=new MockResponse();
    }
    public async GetIllustrations() {

        var educareResponse = this._mockResponse.getEducareMockResponse(15, this._vpmsRequest["applicationId"]);
        /** First Life Assured**/
        var runtimeRequestLA1 = this._runtimeRequestMapper.getEducareIllustrationsRequest(this._vpmsRequest,this._sharedEntities.illustrationType);
        var getIllustrationsResponse =await callRuntime(runtimeRequestLA1.body,this._dataConnector);
        educareResponse = this._responseMapper.mapEducareResponse(educareResponse,getIllustrationsResponse,this._sharedEntities.illustrationType);
        /** Second Life Assured**/
        if(this._vpmsRequest["secondlifeAssured"]["dateOfBirth"]!=null)
        {
            this._sharedEntities.illustrationType=IllustrationType.LA2;
            var runtimeRequestLA2 = this._runtimeRequestMapper.getEducareIllustrationsRequest(this._vpmsRequest,this._sharedEntities.illustrationType);
            var getIllustrationsResponse =await callRuntime(runtimeRequestLA2.body,this._dataConnector);
            educareResponse = this._responseMapper.mapEducareResponse(educareResponse,getIllustrationsResponse,this._sharedEntities.illustrationType);
        }
        return educareResponse;
    }
}
async function callRuntime(request: any,dataConnector:any):Promise<any> {
    try{
    return new ProductMasterFactory("CUSTOM", dataConnector)
        .getProductComputeRulesService()
        .calculate(request, {
            ruleType: "ILLUSTRATIONS"
        });
    }
    catch(err)
    {
        return err;
    }
}