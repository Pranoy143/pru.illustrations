import { runtimeFacade } from "../runtimeFacade"
import { getPremiumRequest } from "./debug/getPremiumRequest.js"
import { dataConnector } from "./data-connector";
import { TestEntities } from "../common/testEntities";
import { fileSystemHelper } from "./service-helper/fileSystem-helper";
let fileHelper=new fileSystemHelper();
let request = getPremiumRequest();
let runtimefacade = new runtimeFacade(request.body, dataConnector);

/**********************************INPUT NEEDED FOR TEST**********************************/
let applicableTests = ["PREMIUM", "GETPLANS"];
const filePath="C:\\Users\\pranoy.ghosh\\Documents\\NPMPACKAGETEST";
/**********************************INPUT NEEDED FOR TEST**********************************/
applicableTests.forEach(test => {
    let callBackPromise = null;
    let testResult = new TestEntities(test.toUpperCase());
    if (test.toUpperCase() === "PREMIUM") {
        callBackPromise = runtimefacade.GetPremium();
        callBackPromise.then(res => {
            testResult.response = res;           
            fileHelper.WriteFile(filePath,testResult);
        })
    }
    else if (test.toUpperCase() === "GETPLANS") {
        callBackPromise = runtimefacade.GetPlans();
        callBackPromise.then(res => {
            testResult.response = res;
            fileHelper.WriteFile(filePath,testResult);
        })
    }
});

