const axios = require('axios');
const { ProductMasterFactory } = require("@product-master-sdk/product-master-lib");
import {HttpHelper} from "./interface/httpHelper";

export class productDefinitionHelper implements HttpHelper{
    async GetSummarisedProductDefs(lbu: string, code: string)
    {
        let summarisedProductDefs=[];
        let baseProductResponse=await this.GetProductData(lbu,code);
        summarisedProductDefs.push(baseProductResponse);
        for await (const component of baseProductResponse?.components) {
            let compResponse=await this.GetProductData(lbu,component.code);
            if(compResponse){
                summarisedProductDefs.push(compResponse);
            }
        }
        return summarisedProductDefs;
    }

    async  GetProductData(lbu: string, code: string) {
        let prodDataUrl=`http://dev1-product-master-sdk-node-apis.azurewebsites.net/product-service/products/DATA/${lbu}/${code}`;
        let response=await axios.get(prodDataUrl);
        return response.data.body;
    }

    async  callRuntime(request: any, dataConnector: any, ruleType: boolean, name: string,prodDefs?:any): Promise<any> {
        try {
            if (ruleType) {
                return new ProductMasterFactory("CUSTOM", dataConnector)
                    .getProductComputeRulesService()
                    .calculate(request, {
                        ruleType: name,
                        ruleName: undefined
                    },prodDefs);
            }
            else {
                return new ProductMasterFactory("CUSTOM", dataConnector)
                    .getProductComputeRulesService()
                    .calculate(request, {
                        ruleType: "EXTERNAL",
                        ruleName: name
                    },prodDefs);
            }
        }
        catch (err) {
            return err;
        }
    }
}