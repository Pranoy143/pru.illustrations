import { FileHelper } from "./interface/fileHelper";
const fs = require("mz/fs");
const path = require("path");
const sortJson=require('sort-json');

export class fileSystemHelper implements FileHelper {

    async WriteFile(filePath: any, fileContent: any) {
        try 
        {
            const fileName=`${fileContent.responseType}_Response_${new Date().toISOString().split('T')[0]}.json`
            let actualFilePath=path.join(filePath,fileName);        
            let response=await fs.writeFileSync(actualFilePath,JSON.stringify(fileContent,null,"\t"),'utf-8');
            console.log('File Written in ',actualFilePath);
        }
        catch (exception) {
            throw new Error("Method not implemented.");
        }
    }

}