export interface HttpHelper{
    GetSummarisedProductDefs(lbu: string, code: string):any;
    callRuntime(request: any, dataConnector: any, ruleType: boolean, name: string,prodDefs?:any):any;
}