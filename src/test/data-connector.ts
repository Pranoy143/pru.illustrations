import axios from "axios";
const setupCache = require("axios-cache-adapter").setupCache;
import { getComputeRules } from "./debug/computeRules.js";
import { getCommonFunction} from "./debug/commonFunction.js";
import { getApplicableTransactions } from "./debug/transactions.js";


const cache = setupCache({
    maxAge: 15 * 60 * 1000
});

const api = axios.create({
    adapter: cache.adapter
});

export const dataConnector = {
    getProductDefinition: (lbu: string, productCode: string) => {
        const url = `https://dev1-product-master-sdk-node-apis.azurewebsites.net/product-service/products/data/${lbu}/${productCode}`;
        return api({ url, method: 'GET' })
            .then((result: { data: { body: any; }; }) => Promise.resolve(result.data.body));
    },
    getProductCatalog: () => { },
    getProductSearchResults: () => { },
    getComputeRules: () => {
        return getComputeRules();
    },

    getCommonFunctions: () => {
        return getCommonFunction()["commonFunction"];
    },

    getDocumentByType: (documentType: string) => {
        let resp = null;
        switch (documentType) {
            case "commonFunction":
                resp = getCommonFunction()["commonFunction"];
                break;
            case "PRODUCT_TRANSACTIONS/APPLICABLE_TRANSACTIONS":
                resp = getApplicableTransactions();
                break;
        }
        return resp;
    }
}
