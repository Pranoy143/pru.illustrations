export function getPremiumRequest() {
    return {
        "body": {
            "transactionContext": {
                "transaction": "SALES",
                "transactionRole": "ANY",
                "transactionChannel": "ALL"
            },
            "products":[
                {
                    "code": "S00530",
                    "attributes": [
                        {
                            "name": "planName",
                            "value": "Basic Benefit Plan"
                        },
                        {
                            "name": "totalSumAssured",
                            "value": 5000000
                        },
                        {
                            "name": "policyTerm",
                            "value": 14
                        },
                        {
                            "name": "premiumFrequency",
                            "value": "Half-Yearly"
                        }
                    ],
                    "components":[
                        {
                            "code": "S00304",
                            "attributes": [
                                {
                                    "name": "birthDate",
                                    "value": "09/04/2002"
                                }
                            ]
                        }
                    ]
                }
            ]
        }

    }
}