export function getApplicableTransactions() {
    return {
        "_id": "PRODUCT_TRANSACTIONS/APPLICABLE_TRANSACTIONS",
        "documentType": "PRODUCT_TRANSACTIONS",
        "channel": "ALL",
        "LBU": "ALL",
        "applicableTransactions": [
            {
                "policyStatus": "INFORCE",
                "transactionsList": [
                    {
                        "transaction": "CHANGEBENEFICIARY",
                        "transactionRole": "PH",
                        "transactionChannel": "PULSE"
                    },
                    {
                        "transaction": "CHANGECONTACTDETAILS",
                        "transactionRole": "PH",
                        "transactionChannel": "PULSE"
                    },
                    {
                        "transaction": "CANCEL",
                        "transactionRole": "PH",
                        "transactionChannel": "PULSE"
                    },
                    {
                        "transaction": "AUTODEBIT",
                        "transactionRole": "ANY",
                        "transactionChannel": "PULSE"
                    },
                    {
                        "transaction": "RENEW",
                        "transactionRole": "PH",
                        "transactionChannel": "PULSE"
                    },
                    {
                        "transaction": "SURRENDER",
                        "transactionRole": "PH",
                        "transactionChannel": "EPOS"
                    },
                    {
                        "transaction": "PAYPREMIUM",
                        "transactionRole": "PH",
                        "transactionChannel": "EPOS"
                    },
                    {
                        "transaction": "MAJORCLAIMS",
                        "transactionRole": "BENEFICARY",
                        "transactionChannel": "PULSE"
                    },
                    {
                        "transaction": "MINORCLAIMS",
                        "transactionRole": "PH",
                        "transactionChannel": "PULSE"
                    },
                    {
                        "transaction": "TOPUP",
                        "transactionRole": "PH",
                        "transactionChannel": "ALL"
                    },
                    {
                        "transaction": "CANCELAUTORENEW",
                        "transactionRole": "ANY",
                        "transactionChannel": "ALL"
                    }
                ]
            },
            {
                "policyStatus": "LAPSED",
                "transactionsList": [
                    {
                        "transaction": "REINSTATEMENT",
                        "transactionRole": "PH",
                        "transactionChannel": "ALL"
                    }
                ]
            }
        ]
    };
}