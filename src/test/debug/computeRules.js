export function getComputeRules() {
    return [
        "PREMIUM",
        "REFUND",
        "BONUS",
        "ILLUSTRATIONS",
        "VALIDATIONS",
        "GIRISH-TEST",
        "TRANSACTIONS",
        "TOPUP",
        "BILLINGPARAMETERS,true",
        "NOTIFICATIONS,true",
        "PAYOUTPARAMETERS,true"
    ];
}