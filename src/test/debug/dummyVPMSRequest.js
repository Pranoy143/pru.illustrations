export function GetDummyVPMSRequest() {
    return {
        "inQuotationStage": false,
        "planCode": "educare",
        "initPaymentMethod": "C",
        "insuredAmount": 4000,
        "period": 15,
        "paymentMode": "1",
        "productMode": "Coverage",
        "periodSafelife": 0,
        "periodPureProtect": 0,
        "periodEasylife": 0,
        "paymentTermEasylife": 0,
        "paymentTerm": 15,
        "currency": "USD",
        "paymentTermSafelife": 0,
        "insuredAmountSafelife": "0",
        "policyOwnerAge": 28,
        "paymentTermPureProtect": 0,
        "extendedCoverageTerm": 0,
        "loanAssurePolicyTerm": null,
        "paymentModeNoun": "Year",
        "inBudget": null,
        "affordabilityVisibilityCheck": null,
        "investmentPriority": 50,

        "assured": {
            "age": 28,
            "dateOfBirth": "1992-02-18",
            "gender": "M"
        },
        "policyHolder": {
            "age": 28,
            "dateOfBirth": "1992-02-18"
        },
        "applicationId": "200000282",
        "secondlifeAssured": {
            "dateOfBirth": "1995-02-18",
            "age": null,
            "gender": "F"
        },
        "fundAllocation": {},
        "riders": {
            "PCB": {
                "educareinsuredAmountLA1": 0,
                "educareinsuredAmountLA2": 0,
                "educarecheckLA1": true,
                "educarecheckLA2": false
            },
            "FIB": {
                "educareinsuredAmountLA1": 0,
                "educareinsuredAmountLA2": 0,
                "educarecheckLA1": true,
                "educarecheckLA2": false
            },
            "PRUT": {
                "extendedCoverageTerm": 15,
                "educareinsuredAmountLA1": 80000,
                "educareinsuredAmountLA2": 80000,
                "educarecheckLA1": true,
                "educarecheckLA2": false
            },
            "CI": {
                "ciType": "AC50",
                "educareccwopLA1": "Included",
                "educareccwopLA2": "Included",
                "pureprotectccwopLA1": "Included",
                "pureprotectccwopLA2": "Included",
                "educareinsuredAmountLA1": 80000,
                "educareinsuredAmountLA2": 80000,
                "educarecheckLA1": true,
                "educarecheckLA2": false
            }
        },
        "topups": [],
        "withdrawals": [],
        "requestedAt": 1582004171219
    };
}