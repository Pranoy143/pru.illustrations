export function GetDummyResponse() {
    return {
        "msgId": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
        "timestamp": "Mon, 02 Mar 2020 08:31:41 GMT",
        "source": "string",
        "correlationId": "string",
        "status": {
            "code": 0
        },
        "body": {
            "transactionContext": {
                "transaction": "SALES",
                "transactionRole": "ANY",
                "transactionChannel": "ALL",
                "id": "SALES-ANY-ALL"
            },
            "products": [{
                "code": "S00471",
                "attributes": [{
                        "name": "premiumFrequency",
                        "value": "Annually"
                    },
                    {
                        "name": "paymentMode",
                        "value": "NonCash"
                    },
                    {
                        "name": "policyTerm",
                        "value": 15
                    },
                    {
                        "name": "totalSumAssured",
                        "value": 4000
                    },
                    {
                        "name": "discountRate",
                        "value": 0.97
                    },
                    {
                        "name": "discountLimitForSA",
                        "value": 30000
                    },
                    {
                        "name": "isAMLRequired",
                        "value": false
                    },
                    {
                        "name": "waitingPeriod",
                        "value": 0
                    },
                    {
                        "name": "ESCIRate",
                        "value": 0.25
                    },
                    {
                        "name": "LSCIRate",
                        "value": 0.75
                    },
                    {
                        "name": "coveragePremium",
                        "value": 0
                    },
                    {
                        "name": "isRefundAllowed",
                        "value": true
                    },
                    {
                        "name": "isSurrenderAllowed",
                        "value": true
                    },
                    {
                        "name": "freelookPeriod",
                        "value": 21
                    },
                    {
                        "name": "claimSubmissionPeriod",
                        "value": 0
                    },
                    {
                        "name": "minSumAssured",
                        "value": 4000
                    }
                ],
                "components": [{
                        "code": "S00304",
                        "attributes": [{
                                "name": "birthDate",
                                "value": "18/02/1992"
                            },
                            {
                                "name": "gender",
                                "value": "M"
                            }
                        ]
                    },
                    {
                        "code": "S00285",
                        "attributes": [{
                                "name": "isSelected",
                                "value": true
                            },
                            {
                                "name": "base_totalSumAssured",
                                "value": 4000
                            },
                            {
                                "name": "base_policyTerm",
                                "value": 15
                            },
                            {
                                "name": "base_paymentMode",
                                "value": "NonCash"
                            },
                            {
                                "name": "base_discountRate",
                                "value": 0.97
                            },
                            {
                                "name": "base_discountLimitForSA",
                                "value": 30000
                            },
                            {
                                "name": "base_premiumFrequency",
                                "value": "Annually"
                            },
                            {
                                "name": "base_LSCIRate",
                                "value": 0.75
                            },
                            {
                                "name": "base_ESCIRate",
                                "value": 0.25
                            },
                            {
                                "name": "base_waitingPeriod",
                                "value": 0
                            }
                        ],
                        "tables": [{
                            "planInfo": [{
                                    "term": 1,
                                    "age": 28,
                                    "premiumValue": 8.55,
                                    "totalPremiumPaid": 8.55,
                                    "totalSumAssured": 4000,
                                    "totalSumAssuredAccident": 8000,
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 2,
                                    "age": 29,
                                    "premiumValue": 8.55,
                                    "totalPremiumPaid": 17.1,
                                    "totalSumAssured": 4000,
                                    "totalSumAssuredAccident": 8000,
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 3,
                                    "age": 30,
                                    "premiumValue": 8.55,
                                    "totalPremiumPaid": 25.65,
                                    "totalSumAssured": 4000,
                                    "totalSumAssuredAccident": 8000,
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 4,
                                    "age": 31,
                                    "premiumValue": 8.55,
                                    "totalPremiumPaid": 34.2,
                                    "totalSumAssured": 4000,
                                    "totalSumAssuredAccident": 8000,
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 5,
                                    "age": 32,
                                    "premiumValue": 8.55,
                                    "totalPremiumPaid": 42.75,
                                    "totalSumAssured": 4000,
                                    "totalSumAssuredAccident": 8000,
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 6,
                                    "age": 33,
                                    "premiumValue": 8.55,
                                    "totalPremiumPaid": 51.3,
                                    "totalSumAssured": 4000,
                                    "totalSumAssuredAccident": 8000,
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 7,
                                    "age": 34,
                                    "premiumValue": 8.55,
                                    "totalPremiumPaid": 59.85,
                                    "totalSumAssured": 4000,
                                    "totalSumAssuredAccident": 8000,
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 8,
                                    "age": 35,
                                    "premiumValue": 8.55,
                                    "totalPremiumPaid": 68.4,
                                    "totalSumAssured": 4000,
                                    "totalSumAssuredAccident": 8000,
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 9,
                                    "age": 36,
                                    "premiumValue": 8.55,
                                    "totalPremiumPaid": 76.95,
                                    "totalSumAssured": 4000,
                                    "totalSumAssuredAccident": 8000,
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 10,
                                    "age": 37,
                                    "premiumValue": 8.55,
                                    "totalPremiumPaid": 85.5,
                                    "totalSumAssured": 4000,
                                    "totalSumAssuredAccident": 8000,
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 11,
                                    "age": 38,
                                    "premiumValue": 8.55,
                                    "totalPremiumPaid": 94.05,
                                    "totalSumAssured": 4000,
                                    "totalSumAssuredAccident": 8000,
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 12,
                                    "age": 39,
                                    "premiumValue": 8.55,
                                    "totalPremiumPaid": 102.6,
                                    "totalSumAssured": 4000,
                                    "totalSumAssuredAccident": 8000,
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 13,
                                    "age": 40,
                                    "premiumValue": 8.55,
                                    "totalPremiumPaid": 111.15,
                                    "totalSumAssured": 4000,
                                    "totalSumAssuredAccident": 8000,
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 14,
                                    "age": 41,
                                    "premiumValue": 8.55,
                                    "totalPremiumPaid": 119.7,
                                    "totalSumAssured": 4000,
                                    "totalSumAssuredAccident": 8000,
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 15,
                                    "age": 42,
                                    "premiumValue": 8.55,
                                    "totalPremiumPaid": 128.25,
                                    "totalSumAssured": 4000,
                                    "totalSumAssuredAccident": 8000,
                                    "surrenderValue": "0"
                                }
                            ]
                        }]
                    },
                    {
                        "code": "S00286",
                        "attributes": [{
                                "name": "isSelected",
                                "value": true
                            },
                            {
                                "name": "base_policyTerm",
                                "value": 15
                            },
                            {
                                "name": "base_paymentMode",
                                "value": "NonCash"
                            },
                            {
                                "name": "base_totalSumAssured",
                                "value": 4000
                            },
                            {
                                "name": "base_LSCIRate",
                                "value": 0.75
                            },
                            {
                                "name": "base_ESCIRate",
                                "value": 0.25
                            },
                            {
                                "name": "base_discountRate",
                                "value": 0.97
                            },
                            {
                                "name": "base_discountLimitForSA",
                                "value": 30000
                            },
                            {
                                "name": "base_waitingPeriod",
                                "value": 0
                            },
                            {
                                "name": "base_premiumFrequency",
                                "value": "Annually"
                            }
                        ],
                        "tables": [{
                            "planInfo": [{
                                    "term": 1,
                                    "age": 28,
                                    "premiumValue": 10.51,
                                    "totalPremiumPaid": 10.51,
                                    "totalSumAssured": 4000,
                                    "totalSumAssuredAccident": 8000,
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 2,
                                    "age": 29,
                                    "premiumValue": 10.51,
                                    "totalPremiumPaid": 21.02,
                                    "totalSumAssured": 4000,
                                    "totalSumAssuredAccident": 8000,
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 3,
                                    "age": 30,
                                    "premiumValue": 10.51,
                                    "totalPremiumPaid": 31.53,
                                    "totalSumAssured": 4000,
                                    "totalSumAssuredAccident": 8000,
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 4,
                                    "age": 31,
                                    "premiumValue": 10.51,
                                    "totalPremiumPaid": 42.04,
                                    "totalSumAssured": 4000,
                                    "totalSumAssuredAccident": 8000,
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 5,
                                    "age": 32,
                                    "premiumValue": 10.51,
                                    "totalPremiumPaid": 52.55,
                                    "totalSumAssured": 4000,
                                    "totalSumAssuredAccident": 8000,
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 6,
                                    "age": 33,
                                    "premiumValue": 10.51,
                                    "totalPremiumPaid": 63.06,
                                    "totalSumAssured": 4000,
                                    "totalSumAssuredAccident": 8000,
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 7,
                                    "age": 34,
                                    "premiumValue": 10.51,
                                    "totalPremiumPaid": 73.57,
                                    "totalSumAssured": 4000,
                                    "totalSumAssuredAccident": 8000,
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 8,
                                    "age": 35,
                                    "premiumValue": 10.51,
                                    "totalPremiumPaid": 84.08,
                                    "totalSumAssured": 4000,
                                    "totalSumAssuredAccident": 8000,
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 9,
                                    "age": 36,
                                    "premiumValue": 10.51,
                                    "totalPremiumPaid": 94.59,
                                    "totalSumAssured": 4000,
                                    "totalSumAssuredAccident": 8000,
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 10,
                                    "age": 37,
                                    "premiumValue": 10.51,
                                    "totalPremiumPaid": 105.1,
                                    "totalSumAssured": 4000,
                                    "totalSumAssuredAccident": 8000,
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 11,
                                    "age": 38,
                                    "premiumValue": 10.51,
                                    "totalPremiumPaid": 115.61,
                                    "totalSumAssured": 4000,
                                    "totalSumAssuredAccident": 8000,
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 12,
                                    "age": 39,
                                    "premiumValue": 10.51,
                                    "totalPremiumPaid": 126.12,
                                    "totalSumAssured": 4000,
                                    "totalSumAssuredAccident": 8000,
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 13,
                                    "age": 40,
                                    "premiumValue": 10.51,
                                    "totalPremiumPaid": 136.63,
                                    "totalSumAssured": 4000,
                                    "totalSumAssuredAccident": 8000,
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 14,
                                    "age": 41,
                                    "premiumValue": 10.51,
                                    "totalPremiumPaid": 147.14,
                                    "totalSumAssured": 4000,
                                    "totalSumAssuredAccident": 8000,
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 15,
                                    "age": 42,
                                    "premiumValue": 10.51,
                                    "totalPremiumPaid": 157.65,
                                    "totalSumAssured": 4000,
                                    "totalSumAssuredAccident": 8000,
                                    "surrenderValue": "0"
                                }
                            ]
                        }]
                    },
                    {
                        "code": "S00287",
                        "attributes": [{
                                "name": "totalSumAssured",
                                "value": 4000
                            },
                            {
                                "name": "isSelected",
                                "value": true
                            },
                            {
                                "name": "base_premiumFrequency",
                                "value": "Annually"
                            },
                            {
                                "name": "base_policyTerm",
                                "value": 15
                            },
                            {
                                "name": "base_paymentMode",
                                "value": "NonCash"
                            },
                            {
                                "name": "base_waitingPeriod",
                                "value": 0
                            },
                            {
                                "name": "base_discountRate",
                                "value": 0.97
                            },
                            {
                                "name": "base_discountLimitForSA",
                                "value": 30000
                            },
                            {
                                "name": "futurePremiumWaiverFlag",
                                "value": true
                            }
                        ],
                        "tables": [{
                            "planInfo": [{
                                    "term": 1,
                                    "age": 28,
                                    "premiumValue": 83.89,
                                    "totalPremiumPaid": 83.89,
                                    "totalSumAssured": "0",
                                    "totalSumAssuredAccident": "0",
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 2,
                                    "age": 29,
                                    "premiumValue": 83.89,
                                    "totalPremiumPaid": 167.78,
                                    "totalSumAssured": "0",
                                    "totalSumAssuredAccident": "0",
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 3,
                                    "age": 30,
                                    "premiumValue": 83.89,
                                    "totalPremiumPaid": 251.67,
                                    "totalSumAssured": "0",
                                    "totalSumAssuredAccident": "0",
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 4,
                                    "age": 31,
                                    "premiumValue": 83.89,
                                    "totalPremiumPaid": 335.56,
                                    "totalSumAssured": "0",
                                    "totalSumAssuredAccident": "0",
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 5,
                                    "age": 32,
                                    "premiumValue": 83.89,
                                    "totalPremiumPaid": 419.45,
                                    "totalSumAssured": "0",
                                    "totalSumAssuredAccident": "0",
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 6,
                                    "age": 33,
                                    "premiumValue": 83.89,
                                    "totalPremiumPaid": 503.34,
                                    "totalSumAssured": "0",
                                    "totalSumAssuredAccident": "0",
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 7,
                                    "age": 34,
                                    "premiumValue": 83.89,
                                    "totalPremiumPaid": 587.23,
                                    "totalSumAssured": "0",
                                    "totalSumAssuredAccident": "0",
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 8,
                                    "age": 35,
                                    "premiumValue": 83.89,
                                    "totalPremiumPaid": 671.12,
                                    "totalSumAssured": "0",
                                    "totalSumAssuredAccident": "0",
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 9,
                                    "age": 36,
                                    "premiumValue": 83.89,
                                    "totalPremiumPaid": 755.01,
                                    "totalSumAssured": "0",
                                    "totalSumAssuredAccident": "0",
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 10,
                                    "age": 37,
                                    "premiumValue": 83.89,
                                    "totalPremiumPaid": 838.9,
                                    "totalSumAssured": "0",
                                    "totalSumAssuredAccident": "0",
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 11,
                                    "age": 38,
                                    "premiumValue": 83.89,
                                    "totalPremiumPaid": 922.79,
                                    "totalSumAssured": "0",
                                    "totalSumAssuredAccident": "0",
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 12,
                                    "age": 39,
                                    "premiumValue": 83.89,
                                    "totalPremiumPaid": 1006.68,
                                    "totalSumAssured": "0",
                                    "totalSumAssuredAccident": "0",
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 13,
                                    "age": 40,
                                    "premiumValue": 83.89,
                                    "totalPremiumPaid": 1090.57,
                                    "totalSumAssured": "0",
                                    "totalSumAssuredAccident": "0",
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 14,
                                    "age": 41,
                                    "premiumValue": 83.89,
                                    "totalPremiumPaid": 1174.46,
                                    "totalSumAssured": "0",
                                    "totalSumAssuredAccident": "0",
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 15,
                                    "age": 42,
                                    "premiumValue": 83.89,
                                    "totalPremiumPaid": 1258.35,
                                    "totalSumAssured": "0",
                                    "totalSumAssuredAccident": "0",
                                    "surrenderValue": "0"
                                }
                            ]
                        }]
                    },
                    {
                        "code": "S00282",
                        "attributes": [{
                                "name": "base_totalSumAssured",
                                "value": 4000
                            },
                            {
                                "name": "isSelected",
                                "value": true
                            },
                            {
                                "name": "base_policyTerm",
                                "value": 15
                            },
                            {
                                "name": "base_premiumFrequency",
                                "value": "Annually"
                            },
                            {
                                "name": "base_paymentMode",
                                "value": "NonCash"
                            },
                            {
                                "name": "base_waitingPeriod",
                                "value": 0
                            },
                            {
                                "name": "policyTermUnit",
                                "value": "Y"
                            }
                        ],
                        "tables": [{
                            "planInfo": [{
                                    "term": 1,
                                    "age": 28,
                                    "premiumValue": 18.54,
                                    "totalPremiumPaid": 18.54,
                                    "totalSumAssured": 4000,
                                    "totalSumAssuredAccident": 8000,
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 2,
                                    "age": 29,
                                    "premiumValue": 18.54,
                                    "totalPremiumPaid": 37.08,
                                    "totalSumAssured": 4000,
                                    "totalSumAssuredAccident": 8000,
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 3,
                                    "age": 30,
                                    "premiumValue": 18.54,
                                    "totalPremiumPaid": 55.62,
                                    "totalSumAssured": 4000,
                                    "totalSumAssuredAccident": 8000,
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 4,
                                    "age": 31,
                                    "premiumValue": 18.54,
                                    "totalPremiumPaid": 74.16,
                                    "totalSumAssured": 4000,
                                    "totalSumAssuredAccident": 8000,
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 5,
                                    "age": 32,
                                    "premiumValue": 18.54,
                                    "totalPremiumPaid": 92.7,
                                    "totalSumAssured": 4000,
                                    "totalSumAssuredAccident": 8000,
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 6,
                                    "age": 33,
                                    "premiumValue": 18.54,
                                    "totalPremiumPaid": 111.24,
                                    "totalSumAssured": 4000,
                                    "totalSumAssuredAccident": 8000,
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 7,
                                    "age": 34,
                                    "premiumValue": 18.54,
                                    "totalPremiumPaid": 129.78,
                                    "totalSumAssured": 4000,
                                    "totalSumAssuredAccident": 8000,
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 8,
                                    "age": 35,
                                    "premiumValue": 18.54,
                                    "totalPremiumPaid": 148.32,
                                    "totalSumAssured": 4000,
                                    "totalSumAssuredAccident": 8000,
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 9,
                                    "age": 36,
                                    "premiumValue": 18.54,
                                    "totalPremiumPaid": 166.86,
                                    "totalSumAssured": 4000,
                                    "totalSumAssuredAccident": 8000,
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 10,
                                    "age": 37,
                                    "premiumValue": 18.54,
                                    "totalPremiumPaid": 185.4,
                                    "totalSumAssured": 4000,
                                    "totalSumAssuredAccident": 8000,
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 11,
                                    "age": 38,
                                    "premiumValue": 18.54,
                                    "totalPremiumPaid": 203.94,
                                    "totalSumAssured": 4000,
                                    "totalSumAssuredAccident": 8000,
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 12,
                                    "age": 39,
                                    "premiumValue": 18.54,
                                    "totalPremiumPaid": 222.48,
                                    "totalSumAssured": 4000,
                                    "totalSumAssuredAccident": 8000,
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 13,
                                    "age": 40,
                                    "premiumValue": 18.54,
                                    "totalPremiumPaid": 241.02,
                                    "totalSumAssured": 4000,
                                    "totalSumAssuredAccident": 8000,
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 14,
                                    "age": 41,
                                    "premiumValue": 18.54,
                                    "totalPremiumPaid": 259.56,
                                    "totalSumAssured": 4000,
                                    "totalSumAssuredAccident": 8000,
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 15,
                                    "age": 42,
                                    "premiumValue": 18.54,
                                    "totalPremiumPaid": 278.1,
                                    "totalSumAssured": 4000,
                                    "totalSumAssuredAccident": 8000,
                                    "surrenderValue": "0"
                                }
                            ]
                        }]
                    },
                    {
                        "code": "S00283",
                        "attributes": [{
                                "name": "base_totalSumAssured",
                                "value": 4000
                            },
                            {
                                "name": "isSelected",
                                "value": true
                            },
                            {
                                "name": "base_policyTerm",
                                "value": 15
                            },
                            {
                                "name": "base_paymentMode",
                                "value": "NonCash"
                            },
                            {
                                "name": "base_premiumFrequency",
                                "value": "Annually"
                            },
                            {
                                "name": "base_waitingPeriod",
                                "value": 0
                            },
                            {
                                "name": "policyTermUnit",
                                "value": "Y"
                            }
                        ],
                        "tables": [{
                            "planInfo": [{
                                    "term": 1,
                                    "age": 28,
                                    "premiumValue": 140.73,
                                    "totalPremiumPaid": 140.73,
                                    "totalSumAssured": 4000,
                                    "totalSumAssuredAccident": 8000,
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 2,
                                    "age": 29,
                                    "premiumValue": 140.73,
                                    "totalPremiumPaid": 281.46,
                                    "totalSumAssured": 4000,
                                    "totalSumAssuredAccident": 8000,
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 3,
                                    "age": 30,
                                    "premiumValue": 140.73,
                                    "totalPremiumPaid": 422.19,
                                    "totalSumAssured": 4000,
                                    "totalSumAssuredAccident": 8000,
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 4,
                                    "age": 31,
                                    "premiumValue": 140.73,
                                    "totalPremiumPaid": 562.92,
                                    "totalSumAssured": 4000,
                                    "totalSumAssuredAccident": 8000,
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 5,
                                    "age": 32,
                                    "premiumValue": 140.73,
                                    "totalPremiumPaid": 703.65,
                                    "totalSumAssured": 4000,
                                    "totalSumAssuredAccident": 8000,
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 6,
                                    "age": 33,
                                    "premiumValue": 140.73,
                                    "totalPremiumPaid": 844.38,
                                    "totalSumAssured": 4000,
                                    "totalSumAssuredAccident": 8000,
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 7,
                                    "age": 34,
                                    "premiumValue": 140.73,
                                    "totalPremiumPaid": 985.11,
                                    "totalSumAssured": 4000,
                                    "totalSumAssuredAccident": 8000,
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 8,
                                    "age": 35,
                                    "premiumValue": 140.73,
                                    "totalPremiumPaid": 1125.84,
                                    "totalSumAssured": 4000,
                                    "totalSumAssuredAccident": 8000,
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 9,
                                    "age": 36,
                                    "premiumValue": 140.73,
                                    "totalPremiumPaid": 1266.57,
                                    "totalSumAssured": 4000,
                                    "totalSumAssuredAccident": 8000,
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 10,
                                    "age": 37,
                                    "premiumValue": 140.73,
                                    "totalPremiumPaid": 1407.3,
                                    "totalSumAssured": 4000,
                                    "totalSumAssuredAccident": 8000,
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 11,
                                    "age": 38,
                                    "premiumValue": 140.73,
                                    "totalPremiumPaid": 1548.03,
                                    "totalSumAssured": 4000,
                                    "totalSumAssuredAccident": 8000,
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 12,
                                    "age": 39,
                                    "premiumValue": 140.73,
                                    "totalPremiumPaid": 1688.76,
                                    "totalSumAssured": 4000,
                                    "totalSumAssuredAccident": 8000,
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 13,
                                    "age": 40,
                                    "premiumValue": 140.73,
                                    "totalPremiumPaid": 1829.49,
                                    "totalSumAssured": 4000,
                                    "totalSumAssuredAccident": 8000,
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 14,
                                    "age": 41,
                                    "premiumValue": 140.73,
                                    "totalPremiumPaid": 1970.22,
                                    "totalSumAssured": 4000,
                                    "totalSumAssuredAccident": 8000,
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 15,
                                    "age": 42,
                                    "premiumValue": 140.73,
                                    "totalPremiumPaid": 2110.95,
                                    "totalSumAssured": 4000,
                                    "totalSumAssuredAccident": 8000,
                                    "surrenderValue": "0"
                                }
                            ]
                        }]
                    },
                    {
                        "code": "S00288",
                        "attributes": [{
                                "name": "isSelected",
                                "value": true
                            },
                            {
                                "name": "base_totalSumAssured",
                                "value": 4000
                            },
                            {
                                "name": "base_policyTerm",
                                "value": 15
                            },
                            {
                                "name": "base_paymentMode",
                                "value": "NonCash"
                            },
                            {
                                "name": "base_premiumFrequency",
                                "value": "Annually"
                            },
                            {
                                "name": "base_waitingPeriod",
                                "value": 0
                            }
                        ],
                        "tables": [{
                            "planInfo": [{
                                    "term": 1,
                                    "age": 28,
                                    "premiumValue": 10.51,
                                    "totalPremiumPaid": 10.51,
                                    "totalSumAssured": 4000,
                                    "totalSumAssuredAccident": 8000,
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 2,
                                    "age": 29,
                                    "premiumValue": 10.51,
                                    "totalPremiumPaid": 21.02,
                                    "totalSumAssured": 4000,
                                    "totalSumAssuredAccident": 8000,
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 3,
                                    "age": 30,
                                    "premiumValue": 10.51,
                                    "totalPremiumPaid": 31.53,
                                    "totalSumAssured": 4000,
                                    "totalSumAssuredAccident": 8000,
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 4,
                                    "age": 31,
                                    "premiumValue": 10.51,
                                    "totalPremiumPaid": 42.04,
                                    "totalSumAssured": 4000,
                                    "totalSumAssuredAccident": 8000,
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 5,
                                    "age": 32,
                                    "premiumValue": 10.51,
                                    "totalPremiumPaid": 52.55,
                                    "totalSumAssured": 4000,
                                    "totalSumAssuredAccident": 8000,
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 6,
                                    "age": 33,
                                    "premiumValue": 10.51,
                                    "totalPremiumPaid": 63.059999999999995,
                                    "totalSumAssured": 4000,
                                    "totalSumAssuredAccident": 8000,
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 7,
                                    "age": 34,
                                    "premiumValue": 10.51,
                                    "totalPremiumPaid": 73.57,
                                    "totalSumAssured": 4000,
                                    "totalSumAssuredAccident": 8000,
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 8,
                                    "age": 35,
                                    "premiumValue": 10.51,
                                    "totalPremiumPaid": 84.08,
                                    "totalSumAssured": 4000,
                                    "totalSumAssuredAccident": 8000,
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 9,
                                    "age": 36,
                                    "premiumValue": 10.51,
                                    "totalPremiumPaid": 94.59,
                                    "totalSumAssured": 4000,
                                    "totalSumAssuredAccident": 8000,
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 10,
                                    "age": 37,
                                    "premiumValue": 10.51,
                                    "totalPremiumPaid": 105.10000000000001,
                                    "totalSumAssured": 4000,
                                    "totalSumAssuredAccident": 8000,
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 11,
                                    "age": 38,
                                    "premiumValue": 10.51,
                                    "totalPremiumPaid": 115.61000000000001,
                                    "totalSumAssured": 4000,
                                    "totalSumAssuredAccident": 8000,
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 12,
                                    "age": 39,
                                    "premiumValue": 10.51,
                                    "totalPremiumPaid": 126.12000000000002,
                                    "totalSumAssured": 4000,
                                    "totalSumAssuredAccident": 8000,
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 13,
                                    "age": 40,
                                    "premiumValue": 10.51,
                                    "totalPremiumPaid": 136.63000000000002,
                                    "totalSumAssured": 4000,
                                    "totalSumAssuredAccident": 8000,
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 14,
                                    "age": 41,
                                    "premiumValue": 10.51,
                                    "totalPremiumPaid": 147.14000000000001,
                                    "totalSumAssured": 4000,
                                    "totalSumAssuredAccident": 8000,
                                    "surrenderValue": "0"
                                },
                                {
                                    "term": 15,
                                    "age": 42,
                                    "premiumValue": 10.51,
                                    "totalPremiumPaid": 157.65,
                                    "totalSumAssured": 4000,
                                    "totalSumAssuredAccident": 8000,
                                    "surrenderValue": "0"
                                }
                            ]
                        }]
                    },
                    {
                        "code": "S00309",
                        "attributes": [{
                                "name": "base_totalSumAssured",
                                "value": 4000
                            },
                            {
                                "name": "base_policyTerm",
                                "value": 15
                            },
                            {
                                "name": "base_waitingPeriod",
                                "value": 0
                            },
                            {
                                "name": "base_coveragePremium",
                                "value": 0
                            },
                            {
                                "name": "base_claimSubmissionPeriod",
                                "value": 0
                            },
                            {
                                "name": "claimantRole",
                                "value": "Beneficiary"
                            }
                        ]
                    },
                    {
                        "code": "S00312",
                        "attributes": [{
                                "name": "base_policyTerm",
                                "value": 15
                            },
                            {
                                "name": "base_totalSumAssured",
                                "value": 4000
                            },
                            {
                                "name": "base_waitingPeriod",
                                "value": 0
                            },
                            {
                                "name": "base_claimSubmissionPeriod",
                                "value": 0
                            },
                            {
                                "name": "claimantRole",
                                "value": "Life Insured"
                            }
                        ]
                    }
                ],
                "tables": [{
                    "planInfo": [{
                            "term": 1,
                            "age": 28,
                            "premiumValue": 256.76,
                            "totalSumAssured": 4000,
                            "totalPremiumPaid": 256.76,
                            "totalSumAssuredAccident": 8000,
                            "surrenderValue": 0
                        },
                        {
                            "term": 2,
                            "age": 29,
                            "premiumValue": 256.76,
                            "totalSumAssured": 4000,
                            "totalPremiumPaid": 513.52,
                            "totalSumAssuredAccident": 8000,
                            "surrenderValue": 0
                        },
                        {
                            "term": 3,
                            "age": 30,
                            "premiumValue": 256.76,
                            "totalSumAssured": 4000,
                            "totalPremiumPaid": 770.28,
                            "totalSumAssuredAccident": 8000,
                            "surrenderValue": 31.44
                        },
                        {
                            "term": 4,
                            "age": 31,
                            "premiumValue": 256.76,
                            "totalSumAssured": 4000,
                            "totalPremiumPaid": 1027.04,
                            "totalSumAssuredAccident": 8000,
                            "surrenderValue": 83.84
                        },
                        {
                            "term": 5,
                            "age": 32,
                            "premiumValue": 256.76,
                            "totalSumAssured": 4000,
                            "totalPremiumPaid": 1283.8,
                            "totalSumAssuredAccident": 8000,
                            "surrenderValue": 157.2
                        },
                        {
                            "term": 6,
                            "age": 33,
                            "premiumValue": 256.76,
                            "totalSumAssured": 4000,
                            "totalPremiumPaid": 1540.56,
                            "totalSumAssuredAccident": 8000,
                            "surrenderValue": 251.52
                        },
                        {
                            "term": 7,
                            "age": 34,
                            "premiumValue": 256.76,
                            "totalSumAssured": 4000,
                            "totalPremiumPaid": 1797.32,
                            "totalSumAssuredAccident": 8000,
                            "surrenderValue": 366.8
                        },
                        {
                            "term": 8,
                            "age": 35,
                            "premiumValue": 256.76,
                            "totalSumAssured": 4000,
                            "totalPremiumPaid": 2054.08,
                            "totalSumAssuredAccident": 8000,
                            "surrenderValue": 628.8
                        },
                        {
                            "term": 9,
                            "age": 36,
                            "premiumValue": 256.76,
                            "totalSumAssured": 4000,
                            "totalPremiumPaid": 2310.84,
                            "totalSumAssuredAccident": 8000,
                            "surrenderValue": 825.3
                        },
                        {
                            "term": 10,
                            "age": 37,
                            "premiumValue": 256.76,
                            "totalSumAssured": 4000,
                            "totalPremiumPaid": 2567.6,
                            "totalSumAssuredAccident": 8000,
                            "surrenderValue": 1048
                        },
                        {
                            "term": 11,
                            "age": 38,
                            "premiumValue": 256.76,
                            "totalSumAssured": 4000,
                            "totalPremiumPaid": 2824.36,
                            "totalSumAssuredAccident": 8000,
                            "surrenderValue": 1441
                        },
                        {
                            "term": 12,
                            "age": 39,
                            "premiumValue": 256.76,
                            "totalSumAssured": 4000,
                            "totalPremiumPaid": 3081.12,
                            "totalSumAssuredAccident": 8000,
                            "surrenderValue": 1965
                        },
                        {
                            "term": 13,
                            "age": 40,
                            "premiumValue": 256.76,
                            "totalSumAssured": 4000,
                            "totalPremiumPaid": 3337.88,
                            "totalSumAssuredAccident": 8000,
                            "surrenderValue": 2469.35
                        },
                        {
                            "term": 14,
                            "age": 41,
                            "premiumValue": 256.76,
                            "totalSumAssured": 4000,
                            "totalPremiumPaid": 3594.64,
                            "totalSumAssuredAccident": 8000,
                            "surrenderValue": 3117.8
                        },
                        {
                            "term": 15,
                            "age": 42,
                            "premiumValue": 256.76,
                            "totalSumAssured": 4000,
                            "totalPremiumPaid": 3851.4,
                            "totalSumAssuredAccident": 8000,
                            "surrenderValue": 4000
                        }
                    ]
                }]
            }]
        }
    };
}